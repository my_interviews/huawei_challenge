#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cstdint>
#include <vector>
#include <chrono>
#include <Eigen/Dense>
#include <benchmark/benchmark.h>

Eigen::MatrixXf get_input_data(const std::string& input_data) {
  std::ifstream file(input_data);
  std::string line;
  constexpr auto num_rows = 1797;
  constexpr auto num_cols = 64;
  Eigen::MatrixXf input(num_rows, num_cols);

  uint32_t row_counter = 0;
  while (std::getline(file, line) && row_counter < num_rows) {
    uint32_t col_counter = 0;
    std::stringstream line_ss;
    line_ss << line;        
    std::string element;
    while (std::getline(line_ss, element, ',') && col_counter < num_cols) {
      input(row_counter, col_counter) = std::stod(element);
      col_counter++;
    }
    row_counter++;
  }
  return input;
}

Eigen::MatrixXf get_weights_matrix(const std::string& input_data) {

  std::ifstream file(input_data);
  std::string line;
  constexpr auto num_rows = 64;
  constexpr auto num_cols = 10;
  Eigen::MatrixXf w(num_rows, num_cols);

  uint32_t row_counter = 0;
  while (std::getline(file, line) && row_counter < num_rows) {
    uint32_t col_counter = 0;
    std::stringstream line_ss;
    line_ss << line;        
    std::string element;
    while (std::getline(line_ss, element, ',')) {
      w(row_counter, col_counter) = std::stod(element);
      col_counter++;
    }
    row_counter++;
  }
  return w;
}

Eigen::MatrixXf get_bias_matrix(const std::string& input_data) {

  std::ifstream file(input_data);
  std::string line;
  constexpr auto num_rows = 1;
  constexpr auto num_cols = 10;
  Eigen::MatrixXf b(num_rows, num_cols);

  constexpr auto bias_mx_line_num = 64;
  uint32_t row_counter = 0;
  while (std::getline(file, line)) {
    if (row_counter == bias_mx_line_num) {
      uint32_t col_counter = 0;
      std::stringstream line_ss;
      line_ss << line;        
      std::string element;
      while (std::getline(line_ss, element, ',')) {
        b(0, col_counter) = std::stod(element);
        col_counter++;
      }
    }
    row_counter++;
  }
  return b;
}

static void BM_Classifier(benchmark::State& state) {
  
  const std::string data_file = "../data/digits_1.csv";
  const auto input_data = get_input_data(data_file);

  const std::string weights_file = "../data/weights.csv";
  const auto weights_matrix = get_weights_matrix(weights_file);
  const auto bias_matrix = get_bias_matrix(weights_file);

  for (auto _ : state) {
    const auto repliacte = bias_matrix.colwise().replicate(1797);
    Eigen::MatrixXf res;
    res = input_data * weights_matrix;
    res += repliacte;
    benchmark::DoNotOptimize(res);
  }
}
BENCHMARK(BM_Classifier);
BENCHMARK_MAIN();
