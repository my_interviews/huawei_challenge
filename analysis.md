# Analysis

In this short blog I'm gonna try to analyze how we can improve the original C++ implementation of the neural net classifier while taking advantage of SIMD extensions and parallelism that modern CPUs provide.

# Selecting a Base line

In the [original implementation](https://gitlab.com/my_interviews/huawei_challenge/-/blob/01e4fc26092d7f2a847753d46af248b005c559ed/src/huawei.cpp#L89-95) the input, weights and bias data from the csv files were first read. Then Eigen matrices were instantiated and then the classification problem was solved by manually looping over the wight and input matrices. There are no threads and no explicit SIMD instructions. We're going to choose this implementation as our base line and then compare new changes with this base line.

# Base line analysis

We need to measure how long it takes to solve the problem. In order to do that we simply query the time before and after solving the problem and the difference would be used as our base line measurement. Measuring how long it takes to solve the multiplication just once is not enough, we're gonna solve the problem enough times until the elapsed time over x number of iterations converges to the same number. On my machine 100 iterations is good enough for convergence. So the code snippet that we are trying to measure woul be:

<div align="center">

  <img src="../extra/original_impl.png?raw=true"/>
</div>

Compiling the code in with gcc 5.5 and in release mode with `-O3`; it takes 1061 [µs] to solve the problem, this `1061 micro seconds` would is our base line. It's not too bad doesn't look great either.

# SIMD and vectorization

GCC reports missed optimization opportunities from vectorization passes using the `-fopt-info-vec-missed` flag. Adding this flags and recompiling the binary the compiler generates a long list of lines where it could not vectorize the code. By looking at the assembly code we can also figure out if the compiler is generating any SIMD code instructions or not. Looking into the assembly with `awk` we can see that there only a handful of SSE/SSE2 instructions and no SSE3/SSE4 or AVX instructions in the generated code. This problem is two folds, the first one being we're manually multiplying and adding matrices in a for loop and the second being that we're not asking the compiler what kind of vectorization we expect it to generate.

In the original implementation the classification problem is solved by manually looping over the weight and input matrices and multiplying them and then manually adding the new vector with the bias. The `res` stores the result for the corresponding row in the input data. To avoid these manual loops we can use a matrix only operation. If we replicate the bias matrix so that it becomes a `[1797 x 10]` matrix where every row is a duplicate of the original bias vector we can significantly simplify the solution.

<div align="center">

  <img src="../extra/all_matrix.png?raw=true"/>
</div>

Recompiling and rerunning the above snippet we can see a x3.7 speed up, it now takes `285[µs]` on average to solve the problem.

Looking at the compiler output, there are still a ton of missed optimization opportunities. The number of SSE/SSE2 instructions have grown but there are still no SSE3, SSE4 or AVX instructions in the generated binary. [My CPU](https://ark.intel.com/content/www/us/en/ark/products/125039/intel-xeon-w-2104-processor-8-25&nbsp;m-cache-3-20-ghz.html) supports these instructions, so I probably have to manually ask the compiler to generate code that can take advantage os the SIMD extensions. Looking up gcc's x86 doc there is a long list of flags that can generated code that targets different SIMD extensions; as a general test I tried the following flags

| Compile Flag | Time |
|:---------:|:---------:|
| `-mavx512f`| 265 µs |
| `-msse4` | 240 µs |
| `-msse3` | 237 µs |

So on my machine, GCC 5.5 generates SSE3 and SSE4 instructions outperforms AVX SIMD extension.

## Different compilers and newer releases

Usually newer release of gcc and clang use more recent optimization techniques. The table below shows how the performance changes with different compilers and flags.

| | GCC 9.1 | Clang 9.0 |
| :---------: | :---------:| :---------: |
| `-mavx512f` | 271 µs | 260 µs |
| `-msse4` | 243 µs | 232 µs |
| `-msse3` | 244 µs | 232 µs |

So the best combination seems to be coming from Clang 9.0 and SSE3/4 extensions.

# Threading

After removing the for loops from the algorithm we've lost the control to multi-threading to solve the problem concurrently. This is only partially true; we can no longer use threads in the front end but there is a possibility yo use threads in the backend. Eigen library does support swapping _some_ built-in algorithms with another library as ites backed, such as [MKL](https://software.intel.com/en-us/mkl). 

Rebuilding and rerunning the program with the MKL backend the first noticeable change is how 1000 iterations does not generate a converged result anymore. I had to increase the count to 10,000 iterations to get a more consistent result (This is somewhat expected, multi threading usually introduces indeterminism and instability to the system). To confirm that the program is indeed using all cores on my machine we could use `htop` to monitor the process

<div align="center">

  <img src="../extra/htop.png?raw=true"/>
</div>

As it can be seen from the htop output the process is using all 4 cores on my machine to solve the matrix multiplication problem. We would expect to see a further 3-4x improvement in the performance because after all we're using 4 times more computing power in terms of resources. Well, the disappointing news is that parallelism in this scenario has actually made no difference ot even worse in some cases:

| | GCC 9.1 | Clang 9.0 |
| :---------: | :---------:| :---------: |
| `-mavx512f` | 271 µs | 287 µs |
| `-msse4` | 249 µs | 235 µs |
| `-msse3` | 239 µs | 235 µs |

Is this surprising? Not really. We can't simply use parallelism and expect to see magical 3-4 x time improvements. There are different explanations that can explain the behaviour; use of synchronization primitives, not large enough matrices, expensive run time overhead of creating threads, etc. 

We can try to have a better idea of what's going on by using a performance analysis tool and profile the run time behaviour. Here is a screenshot of what `perf` reports when we run the parallelized MKL backend:

<div align="center">

  <img src="../extra/perf.png?raw=true"/>
</div>

As reported by `perf` a significant number of CPU cycles the `kmp_hyper_barrier_release` call was executing, probably a spin lock waiting for a mutex or semaphore to be released. 


# 32-bit vs 64-bit binaries

In the past couple of years all major tools and applications have switched to use and compile 64 bit applications. This is necessary for a lot of applications but there are still valid reasons to use a 32-bit binary, a lot of embedded applications. Recompiling with 32 bit flags this is how the numbers have changed:

|              | GCC 9.1 | Clang 9.0 |
| :---------: | :---------:| :---------: |
| `-mavx512f` | 132 µs | 130 µs |
| `-msse4` | 156 µs | 151 µs |
| `-msse3` | 156 µs | 151 µs |

Overall there's no significant difference between gcc and clang but in 32 bit compile mode the AVX extension is performing better than SSE3/SSE4 (This might be that in 32 bit mode, there is more data that can fit in the AVX specific registers as opposed to 64 bit version).

# Google benchmark

Although manually specifying the number of iteration loops to measure the performance of the implementation might work it's not ideal. One of the best tools I know of that helps with automating microbenchmarking is the google benchmark library. I've integrated it with this implementation and have created a gitlab CI pipeline with a matrix of different compilers (gcc vs clang), different SIMD extensions (SSE3, SSE4, AVX) and parallelization (Intel MKL) to build and run the benchmark:

<div align="center">

  <img src="../extra/google_benchmark.png?raw=true"/>
</div>

# Conclusion

Here I briefly discussed different methods that can be used to use SIMD and vectorization power of modern CPUs without explicitly writing SIMD intrinsics. In this short blod I showed how compilers can be used to take advantage of automatic vectorization and improve performance by a factor of ~8 __from 1061µs to 130µs__. I also showed how parallelism can have no or negative impact on a seemingly parallelisable data.