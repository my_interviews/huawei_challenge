Please also read the [analysis.md](./analysis.md) file and checkout the pipeline page [here](https://gitlab.com/my_interviews/huawei_challenge/pipelines/135273810).

# Requirements

- CMake version 3.16
  * Follow the instructions [here](https://apt.kitware.com/) to add the CMake APT repo to your machine and install latest CMake

# Build

- Create a build directory `mkdir build && cd build`
- Invoke cmake to generate make files `cmake ../ -DCMAKE_BUILD_TYPE=Release -DBENCHMARK_ENABLE_TESTING=OFF ` # This will take a while as it downloads and installs a couple of dependencies
- Build with make `make -j4`

# Run

From the build directory execute `./huawei_challenge`
